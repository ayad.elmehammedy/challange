package de.imedia24.shop.db.entity

import lombok.*
import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.ZonedDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Data
@Table(name = "products")
data class ProductEntity(
        @Id
        @Column(name = "sku", nullable = false)
        val sku: String,

        @Column(name = "name", nullable = false)
        var name: String,

        @Column(name = "description")
        var description: String? = null,

        @Column(name = "stock")
        val stock: String,

        @Column(name = "price", nullable = false)
        var price: BigDecimal,

        @UpdateTimestamp
        @Column(name = "created_at", nullable = false)
        val createdAt: ZonedDateTime,

        @UpdateTimestamp
        @Column(name = "updated_at", nullable = false)
        var updatedAt: ZonedDateTime
)
