package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

import java.util.Objects.isNull

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/product/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @GetMapping("/products", produces = ["application/json;charset=utf-8"])
    fun findProductsBySkus(@RequestParam("skus") skus: List<String>
    ): ResponseEntity<List<ProductResponse>> {
        logger.info("Request for products $skus")
        return ResponseEntity.ok(productService.findProductsBySkuList(skus));
    }

    @PostMapping("/product", produces = ["application/json;charset=utf-8"])
    fun addProduct(
            @RequestBody product: ProductResponse
    ): ResponseEntity<ProductResponse> {
        logger.info("Adding a  new  product $product")
        return ResponseEntity(productService.createOrUpdate(product), HttpStatus.CREATED);
    }

    @PatchMapping("/product/{sku}", produces = ["application/json;charset=utf-8"])
    fun update(
            @RequestBody product: Map<String, Any?>,
            @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse?> {
        logger.info("Update partially a product")
        val result = productService.update(product, sku);
        if (isNull(result)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(result!!);
    }
}
