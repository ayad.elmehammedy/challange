package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductEntity
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import lombok.AllArgsConstructor
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.ZonedDateTime
import java.util.Objects.nonNull

@Service
@AllArgsConstructor
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductResponse? {
        val product = productRepository.findBySku(sku);
        if (nonNull(product)) {
            return product?.toProductResponse()
        }
        return null;
    }

    fun findProductsBySkuList(skus: List<String>): List<ProductResponse> {
        val productsBySkuList = productRepository.findBySkuIn(skus);
        return productsBySkuList.map { p -> p.toProductResponse() }
    }

    fun createOrUpdate(product: ProductResponse): ProductResponse? {
        val result = productRepository.save(product.toProductEntity())
        return result.toProductResponse()
    }

    fun update(product: Map<String, Any?>, sku: String): ProductResponse? {
        val entity = productRepository.findBySku(sku);
        if(entity != null) {
            product.forEach { entry ->
                when (entry.key) {
                    "name" -> entity.name = entry.value.toString()
                    "description" -> entity.description = entry.value?.toString()
                    "stock" -> entity.description = entry.value?.toString()
                    "price" -> entity.price = BigDecimal(entry.value?.toString());
                    "sku" -> entity.description = entry.value?.toString()
                }
                entity.updatedAt = ZonedDateTime.now()
            }
            val result = productRepository.save(entity);
            return result.toProductResponse();
        }
        return null;
    }

}
